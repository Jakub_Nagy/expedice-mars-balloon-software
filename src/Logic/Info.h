#pragma once

// Project dependencies
#include <Utils.h>
#include <Logic/Packet.h>


class Info {
public:
    // Print current packet data
    static void print_packet()
    {
        println(" ");

        // Air data
        printval("[Info - Air data] DHT11 temperature [°C]: ", packet.temperature_dht);
        printval("[Info - Air data] BMP280 temperature [°C]: ", packet.temperature_bmp);
        printval("[Info - Air data] Humidity [%]: ", packet.humidity);
        printval("[Info - Air data] Heat index [°C]: ", packet.heat_index);
        printval("[Info - Air data] Pressure [hPa]: ", packet.pressure);

        // System data
        printval("[Info - System data] Altitude (calculated by barometric formula) [m]: ", packet.altitude);  
        printval("[Info - System data] Distance measured by ultrasonic sensor [cm]: ", packet.ultrasonic_distance);  

        // Spectral data
        printval("[Info - Spectral data] Ambient light [lux]: ", packet.lux);
        printval("[Info - Spectral data] UV A [uW/m2]: ", packet.uva_lux);
        printval("[Info - Spectral data] UV B [uW/m2]: ", packet.uvb_lux);
        printval("[Info - Spectral data] UV index [index]: ", packet.uv_index);        

        println(" ");
    }
};