#pragma once

// Project dependencies
#include <Utils.h>

// External dependencies
#include <limits.h>


// Packet size definition
#define PACKET_SIZE 20

// Variable type definition
using one_byte = uint8_t;
using two_bytes = uint16_t;
using four_bytes = uint32_t;

template<typename Type>
struct uint_max {};

template<>
struct uint_max<uint8_t> { static constexpr uint8_t value() { return 255U; } };

template<>
struct uint_max<uint16_t> { static constexpr uint16_t value() { return 65535U; } };

template<>
struct uint_max<uint32_t> { static constexpr uint32_t value() { return 4294967295U; } };

template<typename Type, typename As, int64_t MinValue, int64_t MaxValue>
class Compressed {
public:
    Compressed<Type, As, MinValue, MaxValue>& operator=(float value) {
        constexpr double step = (static_cast<double>(MaxValue) - MinValue) / static_cast<double>(uint_max<As>::value());
        data = static_cast<As>(round((static_cast<double>(value) - MinValue) / step));

        return *this;
    }

    operator Type() {
        constexpr double step = (static_cast<double>(MaxValue) - MinValue) / static_cast<double>(uint_max<As>::value());
        return static_cast<Type>(data * step + MinValue);
    }
private:
    As data = 0;
};

// Definition of the data packet
struct Packet 
{
    // Air measurement data
    Compressed<float, two_bytes, -50, 60> temperature_dht;
    Compressed<float, two_bytes, -50, 60> temperature_bmp;
    Compressed<float, one_byte, -50, 60> heat_index;
    Compressed<float, one_byte, 0, 100> humidity;
    Compressed<float, two_bytes, 0, 1100> pressure;

    // System data
    Compressed<float, two_bytes, 0, 50000> altitude;
    Compressed<float, two_bytes, 0, 200> ultrasonic_distance;

    // Spectral data
    Compressed<float, two_bytes, 0, 65536> lux;
    Compressed<float, two_bytes, 0, 65536> uva_lux;
    Compressed<float, two_bytes, 0, 65536> uvb_lux;
    Compressed<float, two_bytes, 0, 65536> uv_index;
};

Packet packet;