// Project dependencies
#include <Utils.h>
#include <Logic/Info.h>
#include <Logic/Packet.h>

// Sensoric Systems
#include <Sensors/HumiditySensor.h>
#include <Sensors/PressureSensor.h>
#include <Sensors/UltrasonicSensor.h>
#include <Sensors/AmbientLightSensor.h>
#include <Sensors/UVSensor.h>

// Power systems
#include <Power/Distribution.h>


void setup()
{
    // Initialize UART interface
    Serial.begin(115200);
    fprintln("\n[System] MCU powered on.");

    // Initialize power distribution systems and turn of VCC1
    Distribution::begin();
}

void loop()
{
    // Enable branch
    Distribution::enable_branch(VCC1);
    
    // Initialize sensor subsystems
    HumiditySensor::begin();
    PressureSensor::begin();
    UltrasonicSensor::begin();
    AmbientLightSensor::begin();
    UVSensor::begin();

    // Read sensor data
    HumiditySensor::read();
    PressureSensor::read();
    UltrasonicSensor::read();
    AmbientLightSensor::read();
    UVSensor::read();

    // Disable branch
    Distribution::disable_branch(VCC1);

    // Print read data
    Info::print_packet();
    
    // Pause for a predefined time period
    fprintln("\n[System] Pausing for 20 seconds before next cycle.\n\n\n");
    delay(20000);
}