#pragma once

// Project dependencies
#include <Utils.h>
#include <Logic/Packet.h>

// External dependencies
#include <DHT.h>


class HumiditySensor {
public:
    // Begin the sensor
    static void begin()
    {
        dht.begin();

        fprintln("[DHT11] Sensor initialized.");
    }

    // Read sensor data
    static void read()
    {
        packet.humidity = dht.readHumidity();
        packet.temperature_dht = dht.readTemperature();
        packet.heat_index = dht.computeHeatIndex(packet.temperature_dht, packet.humidity, false);

        fprintln("[DHT11] Sensor readout complete.");
    }
private:
    // Internal stuff
    static DHT dht;
};

DHT HumiditySensor::dht(7, 11);