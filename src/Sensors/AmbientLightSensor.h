#pragma once

// Project dependencies
#include <Utils.h>

// External dependencies
#include <BH1750.h>


class AmbientLightSensor {
public:
    // Begin the sensor
    static void begin()
    {
        bh1750.begin();

        fprintln("[BH1750] Sensor initialized.");
    }

    // Read sensor data
    static void read()
    {
        packet.lux = bh1750.readLightLevel();

        fprintln("[BH1750] Sensor readout complete.");
    }
private:
    // Internal stuff
    static BH1750 bh1750;
};

BH1750 AmbientLightSensor::bh1750;