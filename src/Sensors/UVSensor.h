#pragma once

// Project dependencies
#include <Utils.h>
#include <Logic/Packet.h>

// External dependencies
#include <VEML6075.h>


class UVSensor {
public:
    // Begin the sensor
    static void begin()
    {
        veml.begin();
        fprintln("[VEML6075] Sensor initialized.\n");
    }

    // Read sensor data
    static void read()
    {
        veml.poll();

        packet.uva_lux = veml.getUVA();
        packet.uvb_lux = veml.getUVB();
        packet.uv_index = veml.getUVIndex();

        fprintln("[VEML6075] Sensor readout complete.\n");
    }
private:
    // Internal stuff
    static VEML6075 veml;
};

VEML6075 UVSensor::veml;