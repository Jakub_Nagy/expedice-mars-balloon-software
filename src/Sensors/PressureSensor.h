#pragma once

// Project dependencies
#include <Utils.h>

// External dependencies
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_BMP280.h>


class PressureSensor {
public:
    // Begin the sensor
    static void begin()
    {
        if (!bmp.begin(BMP280_ADDRESS_ALT, BMP280_CHIPID)) {
            println("Could not find a valid BMP280 sensor, check wiring or try a different address!");
        }

        bmp.setSampling(Adafruit_BMP280::MODE_NORMAL);

        fprintln("[BME280] Sensor initialized.");
    }

    // Read sensor data
    static void read()
    {
        packet.temperature_bmp = bmp.readTemperature();
        packet.pressure = bmp.readPressure() / 100.0f;
        packet.altitude = bmp.readAltitude(1013.25);

        fprintln("[BME280] Sensor readout complete.");
    }
private:
    // Internal stuff
    static Adafruit_BMP280 bmp; 
};

Adafruit_BMP280 PressureSensor::bmp;