#pragma once

// Project dependencies
#include <Utils.h>

// Pin configuration
#define trigPin 2
#define echoPin 3

// Define variables
long duration;
int distance;


class UltrasonicSensor {
public:
    // Begin the sensor
    static void begin()
    {
        pinMode(trigPin, OUTPUT);
        pinMode(echoPin, INPUT);

        fprintln("[HR-SR04] Sensor initialized.");
    }

    // Read sensor data
    static void read()
    {
        digitalWrite(trigPin, LOW);
        delayMicroseconds(5);

        // Trigger the sensor by setting the trigPin high for 10 microseconds:
        digitalWrite(trigPin, HIGH);
        delayMicroseconds(10);
        digitalWrite(trigPin, LOW);

        // Read the echoPin, pulseIn() returns the duration (length of the pulse) in microseconds:
        duration = pulseIn(echoPin, HIGH);

        // Calculate the distance:
        packet.ultrasonic_distance = duration * 0.034 / 2;

        fprintln("[HR-SR04] Sensor readout complete.");
    }
};