#pragma once

#include <Arduino.h>

// This header contains basic functions and utilities and should be included in every file

template <typename T>
static void print(const T& t) {
    Serial.print(t);
}

template <typename T>
static void println(const T& t) {
    Serial.println(t);
}

#define LOGGING_ENABLED

#ifdef LOGGING_ENABLED
    // Used for printing literal strings (like this "Hello, I am a literal string")
    #define fprintln(literal) Serial.println(F(literal));
    #define fprint(literal) Serial.print(F(literal));

    // Print value with a label
    #define printval(literal, arg) { Serial.print(literal); Serial.println(arg); }

    // Print value with a label from flash memory
    #define fprintval(literal, arg) { Serial.print(F(literal)); Serial.println(arg); }

    // Print text and return
    #define fprintreturn(literal) { Serial.print(F(literal)); return; }
#else
    #define fprintln(literal)
    #define fprint(literal)

    #define fprintarg(literal, arg)
     #define fprintreturn(literal)
#endif

// Global loop variable
uint16_t i = 0;