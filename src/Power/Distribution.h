#pragma once

#include <Utils.h>

// Power branch enable pins
#define VCC1_EN_PIN 4

// Power branch definition
#define VCC1 0

 
class Distribution {
public:
    // Initializes the outputs for power distribution
    static void begin()
    {
        pinMode(VCC1_EN_PIN, OUTPUT);
        fprintln("[Power Distribution] System initialized.\n\n");
    }

    // Enables a given power branch
    static void enable_branch(int branch)
    {
        switch(branch)
        {
            case 0:
                digitalWrite(VCC1_EN_PIN, HIGH);
                fprintln("[Power Distribution] VCC1 enabled.\n");
                break;
            default:
                break;
        }
    }

    // Disables a given power branch
    static void disable_branch(int branch)
    {
        switch(branch)
        {
            case 0:
                digitalWrite(VCC1_EN_PIN, LOW);
                fprintln("[Power Distribution] VCC1 disabled.\n");
                break;
            default:
                break;
        }
    }
};